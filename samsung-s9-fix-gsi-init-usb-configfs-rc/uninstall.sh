# remount as read write
remount_rw() {
  mount -o rw,remount /
}

# remount as read only
remount_ro() {
  mount -o rw,remount /
}

local configfs_rc='init.usb.configfs.rc'
local targetfile="/${configfs_rc}"
local bak_targetfile="/bak-${configfs_rc}"

remount_rw

ui_print "Restore ${targetfile}..."
cat "${bak_targetfile}" > "${targetfile}"
rm -vf "${bak_targetfile}"

remount_ro
