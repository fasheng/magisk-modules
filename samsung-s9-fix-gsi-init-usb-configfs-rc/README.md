# Use arm64-aonly init.usb.configfs.rc for GSI roms

Samsung S9 could only flash `arm64-ab` GSI roms but in fact the kernel
use `Android Composite Gadget`(CONFIG_USB_G_ANDROID=y), this module
just copy `init.usb.configfs.rc` from
`Havoc-OS-v3.12-20201230-Official-arm64-aonly` and override it. Magisk
module `nethunter-hid-gadget` only works after this module installed.

**NOTE:** This magisk module will edit/restore your
`/init.usb.configfs.rc` file when executing install/uninstall
functions, and enable/disable module will do nothing.
