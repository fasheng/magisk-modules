#!/usr/bin/env bash

rm -rvf release
mkdir release
for d in *; do
  module_prop="${d}/module.prop"
  if [ -f "${module_prop}" ]; then
    id= version=
    source "${module_prop}" 2>/dev/null
    release_zip="../release/${id}_${version}.zip"
    (
      cd "${d}"
      echo "Generate release/${id}_${version}.zip"
      7z a -bso0 "${release_zip}" *
    )
  fi
done
