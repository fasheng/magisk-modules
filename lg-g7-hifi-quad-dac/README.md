# Fix HiFi Quad DAC for LG G7

## How to use it
After successful installation of this magisk module, you'll see a new
`Hi-Fi Quad DAC` panel in status bar edit mode, drag it to top region,
and insert earphone to active it.
