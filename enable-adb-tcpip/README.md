# Enable ADB TCPIP by default

Set ADB TCPIP default port to 5555 so adbd will listening on TCP port when usb debugging enabled. For Android 11+, user could just ignore the given random port that provided by Wirelss debugging.
