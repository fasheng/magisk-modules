mount -o rw,remount /vendor || mount -o rw,remount /system

ui_print '- Restore /vendor/etc/init/hw/init.lge.usb.rc'
sed -i 's/#\(.*hid.charge\)/\1/' /vendor/etc/init/hw/init.lge.usb.rc

mount -o ro,remount /vendor || mount -o ro,remount /system
