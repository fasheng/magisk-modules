# Replace userdebug and test-keys in fingerprint

Replace userdebug/test-keys keywords to user/release-keys in all
fingerprint keys that defined in build.prop. Including:
- ro.build.description
- ro.build.display.id
- ro.build.fingerprint
- ro.build.flavor
- ro.build.tags
- ro.build.type
- ro.product.build.fingerprint
- ro.product.build.type
- ro.product.build.tags
- ro.system.build.fingerprint
- ro.system.build.type
- ro.system.build.tags
- ro.system_ext.build.fingerprint
- ro.system_ext.build.type
- ro.system_ext.build.tags



