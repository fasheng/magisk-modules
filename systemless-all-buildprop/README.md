# Make all build.prop file be systemless and editable

Copy and override all build.prop files and set them to be editable for
all users. Including:

- /system/build.prop
- /system/product/etc/build.prop
- /system/system_ext/etc/build.prop
- /system/vendor/build.prop
- /system/vendor/odm/etc/build.prop

Inspired by https://github.com/araafroyall/Systemless-build.prop.
