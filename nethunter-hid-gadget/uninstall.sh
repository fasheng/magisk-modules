# remove_section <file> <begin search string> <end search string>
remove_section() {
  local begin endstr last end;
  begin=$(grep -n "$2" $1 | head -n1 | cut -d: -f1);
  if [ "$begin" ]; then
    if [ "$3" == " " -o ! "$3" ]; then
      endstr='^[[:space:]]*$';
      last=$(wc -l $1 | cut -d\  -f1);
    else
      endstr="$3";
    fi;
    for end in $(grep -n "$endstr" $1 | cut -d: -f1) $last; do
      if [ "$end" ] && [ "$begin" -lt "$end" ]; then
        sed -i "${begin},${end}d" $1;
        break;
      fi;
    done;
  fi;
}

# remount as read write
remount_rw() {
  # Remount as read write
  if mountpoint -q /vendor; then
    mount -o rw,remount /vendor
  elif mountpoint -q /system; then
    mount -o rw,remount /system
  elif mountpoint -q /; then
    mount -o rw,remount /
  fi

  if mountpoint -q /vendor/etc; then
    if is_overlay_mountpont /vendor/etc; then
      vendor_etc_dir=$(get_overlay_lowerdir /vendor/etc)
      ui_print "- Found overly mount on /vendor/etc, lowerdir is ${vendor_etc_dir}"
      if mountpoint -q /system; then
        mount -o rw,remount /system
      elif mountpoint -q /; then
        mount -o rw,remount /
      fi
    else
      mount -o rw,remount /vendor/etc
    fi
  fi
}

# remount as read only
remount_ro() {
  # Remount as read only
  if mountpoint -q /vendor; then
    mount -o ro,remount /vendor
  fi
  if mountpoint -q /system; then
    mount -o ro,remount /system
  fi
  if mountpoint -q /; then
    mount -o rw,remount /
  fi
}

hardware=$(getprop ro.hardware)
section_begin='### HID GADGET BEGIN'
section_end='### HID GADGET END'
targetfile=
vendor_dir='/vendor'
vendor_etc_dir='/vendor/etc'

remount_rw

ui_print "- Remove keyboard-descriptor.bin and mouse-descriptor.bin"
rm -rf "${vendor_dir}/keyboard-descriptor.bin"
rm -rf "${vendor_dir}/mouse-descriptor.bin"

targetfile="${vendor_etc_dir}/init/init.nethunter.rc"
if [ -f "${targetfile}" ]; then
  ui_print "- Remove ${targetfile}"
  rm "${targetfile}"
fi

targetfile="${vendor_etc_dir}/init/init.nethunter-android-composite.rc"
if [ -f "${targetfile}" ]; then
  ui_print "- Remove ${targetfile}"
  rm "${targetfile}"
fi

targetfile="${vendor_dir}/ueventd.rc"
ui_print "- Restore ${targetfile}"
remove_section "${targetfile}" "${section_begin}" "${section_end}"

remount_ro
