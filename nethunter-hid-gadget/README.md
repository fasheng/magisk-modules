# Enable Nethunter HID gadget function

## How to use it

1. Press your volume key once to test the input device
1. Choose which usb gadget driver type for your kernel, `Vol+` is the
   default one, and `Vol-` is the older one, which also called
   `Android Composite Gadget` that compiled with the kernel option
   `CONFIG_USB_G_ANDROID=y`.
1. After successful installation of this magisk module, reboot and
   execute `setprop sys.usb.config win,hid,adb`, and there should be
   `/dev/hidgX` devices, then connect usb to PC and apps that support
   gadget will work, such as [USB Keyboard](https://github.com/pelya/android-keyboard-gadget).

**NOTE:** This magisk module will edit/restore your /vendor directory
when executing install/uninstall functions, and enable/disable module
will do nothing.

## Q&A

- Just not working after reboot

  If you don't know your usb gadget driver type, re-install this
  module and test one by one.

- Not working and dmesg shows 'mkdir() failed: Function not implemented'

  Your kernel don't support hid function for configfs, set
  `CONFIG_USB_CONFIGFS_F_HID=y` and recompile the kernel.

- Bootloop problem after reboot

  Maybe your kernel not work with this module, please enter recovery
  mode, mount vendor and system disk and delete
  =/vendor/etc/init/init.nethunter*.rc=, then reboot and you can boot normaly.

  *NOTE:* If your rom mount /vendor/etc as overlay, you could find
  =/vendor/etc/init/init.nethunter*.rc= file in some place like
  =/product/vendor_overlay/29/etc/init/=.
