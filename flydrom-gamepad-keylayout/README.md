# FloydROM keylayout files

Include all /system/usr/keylayout files from FloydROM_T_Exynos9810_Build-20.zip.

### Compatibility:
 - DualShock 2
 - DualShock 3
 - DualShock 4
 - DualSense
 - Xbox 360
 - Xbox One
 - Xbox One S
 - Xbox Series X|S
 - Switch Pro Controller
 - SWITCH JOY-CON
 - Nacon REVOLUTION PRO CONTROLLER
 - Zeemote SteelSeries FREE
